package server

import (
	"encoding/json"
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
)

// AllRooms is the global hashmap for the server
var AllRooms RoomMap

// CreateRoomRequestHandler Create a Room and return roomID
func CreateRoomRequestHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Origin")
	roomID := AllRooms.CreateRoom()

	type resp struct {
		RoomID string `json:"room_id"`
	}

	log.Println(AllRooms.Map)
	json.NewEncoder(w).Encode(resp{RoomID: roomID})
}

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type broadcastMsg struct {
	Message map[string]interface{}
	RoomID  string
	Client  *websocket.Conn
}

var broadcast = make(chan broadcastMsg)

func broadcaster(mutex *sync.Mutex) {
	for {
		msg := <-broadcast
		log.Println("message => ", msg)
		for _, client := range AllRooms.Map[msg.RoomID] {
			if client.Conn != msg.Client {
				mutex.Lock()
				err := client.Conn.WriteJSON(msg.Message)

				if err != nil {
					log.Print("error => ", err)
					client.Conn.Close()
				}
				mutex.Unlock()
			}
		}
	}
}

// JoinRoomRequestHandler will join the client in a particular room
func JoinRoomRequestHandler(w http.ResponseWriter, r *http.Request) {
	roomID, ok := r.URL.Query()["roomID"]

	if !ok {
		log.Println("roomID missing in URL Parameters")
		return
	}

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal("Web Socket Upgrade Error", err)
	}

	AllRooms.InsertIntoRoom(roomID[0], false, ws)

	mutex := new(sync.Mutex)
	// go broadcaster(mutex)

	for {
		var msg broadcastMsg

		err := ws.ReadJSON(&msg.Message)
		if err != nil {
			log.Fatal("Read Error: ", err)
		}

		msg.Client = ws
		msg.RoomID = roomID[0]

		// broadcast <- msg
		log.Println("message => ", msg)
		for _, client := range AllRooms.Map[msg.RoomID] {
			if client.Conn != msg.Client {
				mutex.Lock()
				if err := client.Conn.WriteJSON(msg.Message); err != nil {
					log.Print("error => ", err)
					client.Conn.Close()
				}
				mutex.Unlock()
			}
		}
	}
}
