# Building Video Chat Apps using WebRTC and Golang

Code for the DSC KIIT Workshop "WebRTC + Golang" conducted on 23rd Feb 2021


### Frontend

The `client` is written in React and uses [Vite](https://vitejs.dev/) for the dev server. Run the following commands in the `client` directory

* `npm i` to install all the dependencies
* `npm run dev` to start the local dev server

### Backend

Written in Go. A simple WebSocket server for signalling implemented using 
[gorilla/websocket](https://github.com/gorilla/websocket)

* `go run main.go` to compile and run serve