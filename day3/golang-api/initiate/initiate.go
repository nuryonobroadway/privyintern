package initiate

import (
	"fmt"
	"nuryonobroadway/golang-api/db"
	"nuryonobroadway/golang-api/model"
	"nuryonobroadway/golang-api/server"
	"nuryonobroadway/golang-api/service"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type DbEntry struct {
	conn *gorm.DB
}

func Early() error {
	db, err := dbCreater()
	if err != nil {
		return err
	}

	s := server.NewServer(service.NewService(db))
	s.Route()

	if err := s.Start(); err != nil {
		return err
	}

	return nil
}

func dbCreater() (db.Repository, error) {
	postgresql := "postgresql://root:secret@localhost:5432/books?sslmode=disable"
	conn, err := gorm.Open(postgres.Open(postgresql), &gorm.Config{})
	if err != nil {
		fmt.Println("DB connection error")
		return nil, err
	}

	migration := DbEntry{conn}
	err = migration.migration()
	if err != nil {
		return nil, fmt.Errorf("cannot migrate db: %v", err)
	}

	return db.NewInstance(conn), nil
}

func (d DbEntry) migration() error {
	fmt.Println("migration started")

	// database gorm auto migration struct
	return d.conn.AutoMigrate(
		model.Book{},
	)
}
