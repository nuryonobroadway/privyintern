package model

import (
	"encoding/json"
	"time"
)

type Book struct {
	ID          int
	Title       string
	Description string
	Price       int
	Discount    int
	Rating      int
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type BookRequest struct {
	Title       string      `json:"title" binding:"required"`
	Price       json.Number `json:"price" binding:"required,number"`
	Description string      `json:"description" binding:"required"`
	Discount    json.Number `json:"discount" binding:"required,number"`
	Rating      json.Number `json:"rating" binding:"required,number"`
}

type BookResponse struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Price       int    `json:"price"`
	Discount    int    `json:"discount"`
	Rating      int    `json:"rating"`
}
