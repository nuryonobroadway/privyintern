package db

import (
	"nuryonobroadway/golang-api/model"

	"gorm.io/gorm"
)

type (
	Repository interface {
		FindAll() ([]model.Book, error)
		FindByID(ID int) (model.Book, error)
		Create(book model.Book) (model.Book, error)
		Update(book model.Book) (model.Book, error)
		Delete(book model.Book) (model.Book, error)
	}

	repository struct {
		db *gorm.DB
	}
)

func NewInstance(db *gorm.DB) Repository {
	return &repository{
		db: db,
	}
}
