package db

import "nuryonobroadway/golang-api/model"

func (r *repository) FindAll() ([]model.Book, error) {
	var allBooks []model.Book
	err := r.db.Find(&allBooks).Error
	return allBooks, err
}

func (r *repository) FindByID(ID int) (model.Book, error) {
	var book model.Book
	err := r.db.Where("id = ?", ID).First(&book).Error
	return book, err
}

func (r *repository) Create(book model.Book) (model.Book, error) {
	err := r.db.Create(&book).Error
	return book, err
}

func (r *repository) Update(book model.Book) (model.Book, error) {
	err := r.db.Save(&book).Error

	return book, err
}

func (r *repository) Delete(book model.Book) (model.Book, error) {
	err := r.db.Delete(&book).Error

	return book, err
}
