package service

import (
	"math/rand"
	"nuryonobroadway/golang-api/db"
	"nuryonobroadway/golang-api/model"
)

type (
	Service interface {
		FindAll() ([]model.Book, error)
		FindByID(ID int) (model.Book, error)
		Create(book model.BookRequest) (model.Book, error)
		UpdateByID(ID int, book model.BookRequest) (model.Book, error)
		DeleteByID(ID int) (model.Book, error)
	}

	service struct {
		repository db.Repository
	}
)

func NewService(repository db.Repository) Service {
	return &service{repository}
}

func (s *service) FindAll() ([]model.Book, error) {
	allBooks, err := s.repository.FindAll()
	return allBooks, err
}

func (s *service) FindByID(ID int) (model.Book, error) {
	book, err := s.repository.FindByID(ID)
	return book, err
}

func (s *service) Create(book model.BookRequest) (model.Book, error) {
	price, _ := book.Price.Int64()
	rating, _ := book.Rating.Int64()
	discount, _ := book.Discount.Int64()
	newBook := model.Book{
		ID:          rand.Int(),
		Title:       book.Title,
		Price:       int(price),
		Description: book.Description,
		Discount:    int(discount),
		Rating:      int(rating),
	}
	newBook, err := s.repository.Create(newBook)
	return newBook, err
}

func (s *service) UpdateByID(ID int, bookRequest model.BookRequest) (model.Book, error) {
	book, e := s.repository.FindByID(ID)

	if e != nil {
		return book, e
	}

	price, _ := bookRequest.Price.Int64()
	rating, _ := bookRequest.Rating.Int64()
	discount, _ := bookRequest.Discount.Int64()

	book.Title = bookRequest.Title
	book.Price = int(price)
	book.Description = bookRequest.Description
	book.Discount = int(discount)
	book.Rating = int(rating)

	newBook, err := s.repository.Update(book)
	return newBook, err
}

func (s *service) DeleteByID(ID int) (model.Book, error) {
	book, e := s.repository.FindByID(ID)

	if e != nil {
		return book, e
	}

	newBook, err := s.repository.Delete(book)
	return newBook, err
}
