package main

import (
	"log"
	"nuryonobroadway/golang-api/initiate"
)

func main() {
	if err := initiate.Early(); err != nil {
		log.Fatal(err)
	}
}
