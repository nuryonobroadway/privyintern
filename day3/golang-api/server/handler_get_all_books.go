package server

import (
	"net/http"
	"nuryonobroadway/golang-api/model"

	"github.com/gin-gonic/gin"
)

func (s *Server) GetBookHandler(c *gin.Context) {
	books, err := s.service.FindAll()
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	response := []model.BookResponse{}
	for _, book := range books {
		response = append(response, convertToBookResponse(book))
	}

	// it will return json data
	c.JSON(http.StatusOK, response)
}
