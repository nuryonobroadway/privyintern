package server

import (
	"fmt"
	"nuryonobroadway/golang-api/service"

	"github.com/gin-gonic/gin"
)

type Server struct {
	service service.Service
	router  *gin.Engine
}

func NewServer(service service.Service) *Server {
	return &Server{
		service: service,
		router:  gin.Default(),
	}
}

func (s Server) Route() {
	v1 := s.router.Group("/v1")
	v1.GET("/check", s.checkStatus)
	v1.POST("/books", s.PostBookHandler)
	v1.GET("/books", s.GetBookHandler)
	v1.GET("/books/:id", s.GetBookHandlerByID)
	v1.PUT("/books/:id", s.PutBookHandler)
	v1.DELETE("/books/:id", s.DeleteBookHandler)

}

func (s Server) Start() error {
	fmt.Println("server start on 0.0.0.0:9000")
	return s.router.Run("0.0.0.0:9000")
}
