package server

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (s *Server) GetBookHandlerByID(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	book, err := s.service.FindByID(int(id))
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	// it will return json data
	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
	})
}
