package server

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (s *Server) DeleteBookHandler(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	book, err := s.service.DeleteByID(int(id))
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	res := struct {
		ID       int    `json:"id"`
		Response string `json:"response"`
	}{
		ID:       book.ID,
		Response: "success",
	}

	// it will return json data
	c.JSON(http.StatusOK, res)
}
