package server

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (s *Server) checkStatus(c *gin.Context) {
	res := struct {
		Code     int    `json:"code"`
		Response string `json:"response"`
	}{
		Code:     http.StatusOK,
		Response: "success",
	}

	c.JSON(http.StatusOK, res)
}
