package server

import (
	"fmt"
	"net/http"
	"nuryonobroadway/golang-api/model"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
)

func (s *Server) PutBookHandler(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	var bookRequest model.BookRequest

	// request body should be JSON and same as struct
	err := c.ShouldBindJSON(&bookRequest)
	if err != nil {
		if err, ok := err.(validator.ValidationErrors); ok {
			errorMessages := []string{}
			for _, e := range err {
				errorMessage := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())
				errorMessages = append(errorMessages, errorMessage)
			}

			c.JSON(http.StatusBadRequest, gin.H{
				"errors": errorMessages,
			})
			return
		}
	}

	book, err := s.service.UpdateByID(int(id), bookRequest)
	if err != nil {
		if err.Error() == "record not found" {
			c.JSON(http.StatusNotFound, gin.H{
				"errors": err.Error(),
			})
			return
		}
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": "Wopsss... Internal Server Error",
		})
		return
	}
	// return ok
	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
	})
}
