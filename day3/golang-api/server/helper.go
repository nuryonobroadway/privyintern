package server

import "nuryonobroadway/golang-api/model"

func convertToBookResponse(book model.Book) model.BookResponse {
	return model.BookResponse{
		ID:          book.ID,
		Title:       book.Title,
		Description: book.Description,
		Price:       book.Price,
		Discount:    book.Discount,
		Rating:      book.Rating,
	}
}
