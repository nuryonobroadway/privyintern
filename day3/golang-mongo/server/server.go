package server

import (
	"fmt"
	"net/http"
	"nuryonobroadway/golang-mongo/controllers"

	"github.com/julienschmidt/httprouter"
)

type Server struct {
	r  *httprouter.Router
	uc *controllers.UserController
}

func NewServer(uc *controllers.UserController) Server {
	return Server{
		r:  httprouter.New(),
		uc: uc,
	}
}

func (s Server) Route() {
	s.r.GET("/check", s.uc.CheckStatus)
	s.r.GET("/users/:id", s.uc.GetUser)
	s.r.POST("/users", s.uc.CreateUser)
	s.r.DELETE("/users/:id", s.uc.DeleteUser)

}

func (s Server) Listen() error {
	fmt.Println("server started on port 9000")
	return http.ListenAndServe(":9000", s.r)
}
