package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"nuryonobroadway/golang-mongo/models"

	"github.com/julienschmidt/httprouter"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

type UserController struct {
	session  *mongo.Client
	database *mongo.Collection
}

func NewUserController(dial *mongo.Client, config models.Config) (*UserController, error) {
	return &UserController{
		session:  dial,
		database: dial.Database("materi-a3").Collection("users"),
	}, nil
}

func (uc UserController) CheckStatus(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")

	resp := struct {
		Response string `json:"response"`
	}{
		Response: "success",
	}

	data, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)
}

func (uc UserController) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	if !primitive.IsValidObjectID(id) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	u := models.User{}
	filter := bson.M{"id": oid}
	if err := uc.database.FindOne(context.Background(), filter).Decode(&u); err != nil {
		fmt.Println(err)
		w.WriteHeader(404)
		return
	}

	uj, err := json.Marshal(u)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}

func (uc UserController) CreateUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	u := models.User{}
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	u.Id = primitive.NewObjectID()
	inst, err := uc.database.InsertOne(context.Background(), u)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusNotAcceptable)
		return
	}

	fmt.Println(inst.InsertedID)
	uj, err := json.Marshal(u)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s\n", uj)
}

func (uc UserController) DeleteUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	if !primitive.IsValidObjectID(id) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	filter := bson.M{"id": oid}
	result, err := uc.database.DeleteOne(context.TODO(), filter)
	if err != nil {
		w.WriteHeader(404)
		return
	}

	uj, err := json.Marshal(result)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}
