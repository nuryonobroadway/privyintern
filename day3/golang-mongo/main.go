package main

import (
	"context"
	"fmt"
	"nuryonobroadway/golang-mongo/controllers"
	"nuryonobroadway/golang-mongo/models"
	"nuryonobroadway/golang-mongo/server"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	config, err := models.LoadConfig(".")
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	address := fmt.Sprintf("mongodb://%v:%v", config.MongoDBAddress, config.MongoDBPort)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(address))
	if err != nil {
		panic(err)
	}

	uc, err := controllers.NewUserController(client, config)
	if err != nil {
		panic(err)
	}

	server := server.NewServer(uc)
	server.Route()

	if err := server.Listen(); err != nil {
		panic(err)
	}

	defer func() {
		if err := client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()

}
