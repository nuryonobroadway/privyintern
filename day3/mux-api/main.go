package main

import (
	"log"
	"nuryonobroadway/mux-api/server"
)

func main() {
	// buat instance baru
	s := server.NewServer()

	// panggil method route
	s.Route()

	// mulai server
	if err := s.Start(); err != nil {
		log.Fatal(err)
	}
}
