package server

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// store mux router ke struct
type Server struct {
	r *mux.Router
}

// membuat function NewServer yang return Server
func NewServer() Server {
	return Server{
		// inisialisai router dari mux
		r: mux.NewRouter(),
	}
}

// method yang menampung semua route
func (s Server) Route() {
	s.r.HandleFunc("/check", s.checkStatus).Methods("GET")
	s.r.HandleFunc("/books", s.getBooks).Methods("GET")
	s.r.HandleFunc("/books/{id}", s.getBook).Methods("GET")
	s.r.HandleFunc("/books", s.createBook).Methods("POST")
	s.r.HandleFunc("/books/{id}", s.updateBook).Methods("PUT")
	s.r.HandleFunc("/books/{id}", s.deleteBook).Methods("DELETE")
}

// method untuk memulai api gateway
func (s Server) Start() error {
	log.Println("start HTTP gateway server at 8000")
	return http.ListenAndServe(":8000", s.r)
}
