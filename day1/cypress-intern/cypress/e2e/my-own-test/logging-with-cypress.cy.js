 /// <reference types="cypress" />


// test case dengan nama empty test
describe('Empty test', () => {
    

    // test dengan syntax it 
    it('tes one', () => {
        // visit website
        cy.visit('https://openai.com/blog/')
        
        cy.contains('2022').should('exist')
        cy.contains('Index').should('exist')

        cy.get('.post-card-full.medium-xsmall-copy').each(event => {
            cy.wrap(event).within(() => {
                cy.get(".row > .col-12.col-md-6 > div > h5 > a").then(value => {
                    cy.log(value.text())
                })
            })
        })

    })
})