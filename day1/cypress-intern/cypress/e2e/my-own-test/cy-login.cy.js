 /// <reference types="cypress" />


// test case dengan nama empty test
describe('Empty test', () => {
    

    // test dengan syntax it 
    it('tes one', () => {
        // mengubah tampilan menjadi device iphone x  
        cy.viewport('iphone-x')
        // visit website
        cy.visit('https://openai.com/')

        // mengecek apakah h1 mempunyai text yang sama
        cy.get('.container > .max-width-xnarrow > h1').should('have.text', 'Join us in shaping the future of technology.')

        cy.contains('Log in').click()

        cy.go('back')

        cy.url().then(value => {
            cy.log("The current real url is : ", value)
        })

    })

})