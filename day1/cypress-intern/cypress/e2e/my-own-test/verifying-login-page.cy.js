/// <reference types="cypress" />


// test case dengan nama empty test
describe('Empty test', () => {
    

    // test dengan syntax it 
    it('tes one', () => {

        // visit website
        cy.visit('https://openai.com/')

        // mengecek apakah h1 mempunyai text yang sama
        cy.get('.container > .max-width-xnarrow > h1').should('have.text', 'Join us in shaping the future of technology.')

        // mencari apalah element memiliki text Log in, jika ada 
        // maka akan di ckick
        cy.contains('Log in').click()

        // menegecek apakah element yang dibutuhkan ada
        cy.contains('Welcome back').should('exist')
        cy.contains('Continue with Google').should('exist')
        cy.contains('Continue with Microsoft Account').should('exist')


    })
})