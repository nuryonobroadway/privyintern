/// <reference types="cypress" />


// test case dengan nama empty test
describe('Empty test', () => {
    

    // test dengan syntax it 
    it('tes one', () => {

        // visit website
        cy.visit('https://openai.com/')

        // mengecek apakah h1 mempunyai text yang sama
        cy.get('.container > .max-width-xnarrow > h1').should('have.text', 'Join us in shaping the future of technology.')


    })

    it('test dua', () => {
        cy.visit('www.youtube.com')
    })

})