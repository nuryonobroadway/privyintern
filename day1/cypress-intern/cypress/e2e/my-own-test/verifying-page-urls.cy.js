/// <reference types="cypress" />


// test case dengan nama empty test
describe('Empty test', () => {
    

    // test dengan syntax it 
    it('tes one', () => {

        // visit website
        cy.visit('https://openai.com/')

        // mengecek apakah h1 mempunyai text yang sama
        cy.get('.container > .max-width-xnarrow > h1').should('have.text', 'Join us in shaping the future of technology.')

        // mengecek apakah ada Log in, jika ada lalu click
        cy.contains('Log in').click()

        // mengecek apakah ada Sign up, jika ada lalu click
        cy.contains('Sign up').click()

        // mengecek apakah url memiliki atau mempunyai /signuo
        cy.url().should('include', '/signup')

    })

})