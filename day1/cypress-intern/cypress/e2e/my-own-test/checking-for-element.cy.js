/// <reference types="cypress" />


// test case dengan nama basic test
describe('Basic Test', () => {
    beforeEach(() => {
        cy.visit('https://codedamn.com/playgrounds')

    })

    // test dengan syntax it 
    it('tes one', () => {

        // visit website

        cy.log('click the html playground')

        // //*[@id="layout-conntainer"]/div/main/div/div/section/div/div/div[1]
        cy.get('[data-testid=playground-html]').should('have.text', 'HTML/CSSVanilla HTML/CSS/JS playground')
        // cy.get('.post-card-full.medium-xsmall-copy').each(event => {
        //     cy.wrap(event).within(() => {
        //         cy.get(".row > .col-12.col-md-6 > div > h5 > a").then(value => {
        //             cy.log(value.text())
        //         })
        //     })
        // })
    })

})