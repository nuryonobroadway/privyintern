package main

import "fmt"

func main() {
	// ada beerbagai macam looping
	// salah satunya dengan for atau rekursive
	// golang tidak mendukung while loop maupun do while
	// golang hanya support looping for

	// membuat sample dengan type int
	sample := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}

	// for dengan range sample yang akan mengembalikan value
	// berupa int dan type value dari array
	// int merupakan index
	for _, i := range sample {
		fmt.Print(i)
	}

	fmt.Println()

	// mendefinisikan value berupa stock dengan awal mula looping
	// adalah 0
	stock := 0

	// infinite loop begin here
	for {

		// seleksi jik stock sudah mencapai panjang array dari sample
		// maka for loop akan di break
		// terdapat break dan continue
		// continue berfungsi untuk melanjutkan loop tanpa mengeksekuasi
		// sampai akhir dari for loop
		if stock == len(sample) {
			break
		}

		fmt.Print(sample[stock])

		// menambahkan stock
		stock++
	}

	fmt.Println()

	// for loop biasa seperti pada bahasa pemrograman biasanya
	for i := 0; i < len(sample); i++ {
		fmt.Print(sample[i])
	}

	fmt.Println()

	// menggunakan seleksi apakah i tidak sama dengan panjang dari
	// array
	i := 0
	for i != len(sample) {
		fmt.Print(sample[i])

		// menambahkan i
		i++
	}

	fmt.Println()

	// looping dengan rekursive
	rekursive(sample, 0)

	fmt.Println()

}

func rekursive(sample []int, remain int) {
	// jika remain sama dengan panjang dari array
	// maka akan return
	if remain == len(sample) {
		return
	} else {
		// jika tidak maka akan print value dari array berdasarkan
		// index yang di inisialisasi remain

		fmt.Print(sample[remain])

		// memanggil kembali rekursive sampai return
		rekursive(sample, remain+1)
	}
}
