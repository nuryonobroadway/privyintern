package main

import "fmt"

func main() {

	// set kondisi menjadi true
	condition := true
	if condition {
		fmt.Println("condition: ", condition)
		condition = false
	}

	// deklarasi i == 2
	i := 2

	// cek apakah i same dengan 10
	// jika tidak maka akan di cek kembali apakah i lebih dari 10
	// jika tidak maka akan di cek kembali apakah i kurang dari 10
	if i == 10 {
		fmt.Printf("%v has the same value \n", i)
	} else if i > 10 {
		fmt.Printf("%v more than 10 \n", i)
	} else {
		fmt.Printf("%v less than 10 \n", i)
	}

	// user insert kondisi
	fmt.Print("insert condition: ")
	fmt.Scan(&condition)

	// deklarasi dalam 1 line yang di bedakan dengan deklarasi dalam
	// if yaitu i := condition
	// maka if akan mengecek kondisi setelah ( ; ) apakah bersifat
	// boolean atau tidak
	if i := condition; i {
		fmt.Println("user choose condition to", i)
	}

	// nested loop
	// dimana didalam seleksi terdapat seleksi
	ac := 10
	if ac > 10 {
		if ac > 15 {
			if ac > 20 {
				fmt.Printf("%v more than 20 \n", ac)
			} else {
				fmt.Printf("%v less than 20 \n", ac)
			}
		} else {
			fmt.Printf("%v less than 15 \n", ac)
		}
	} else {
		fmt.Printf("%v less than 10 \n", ac)
	}
}
