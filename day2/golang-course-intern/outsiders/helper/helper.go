package helper

import "fmt"

// function huruf kapital karena function
// bisa di export tidak hanya dapat diakses dari
// package yang sama
func Scan(totalTicket int, again string) int {

	// seleksi jika totalTicket sama dengan 0
	// maka akan return 0
	if totalTicket == 0 {
		fmt.Println("ticket sold out")
		return totalTicket
	}

	// definisi variable username dan ticket
	username := ""
	ticket := 0

	// scan username dan ticket untuk mendapatkan value
	// dari user
	fmt.Print("username: ")
	fmt.Scan(&username)

	fmt.Print("ticket: ")
	fmt.Scan(&ticket)

	// seleksi jika ticket lebih dari totakTicket
	// maka program tidak bisa melanjutkan dan
	// memanggil kembali fungsi scan lagi dengan totalTicket
	// yang sama
	if ticket > totalTicket {
		fmt.Println("ticket remain: ", totalTicket)
		return Scan(totalTicket, again)
	}

	// cek apakah user ingin membeli lagi
	fmt.Print("again: ")
	fmt.Scan(&again)

	// resume akan di set default false
	resume := false

	// jika ada respon yes maka resume menjadi true
	if again == "yes" {
		resume = true
	}

	if !resume {

		// cek kondisi dimana user tidak melanjutkan transaksi
		// maka akan return totalTicket - tiket yang sudah user pesan
		return totalTicket - ticket
	} else {

		// jika user ingin melanjutkan maka dipanggil kembali
		// funsi scan, dengan parameter totalTicket - ticket
		return Scan(totalTicket-ticket, again)
	}
}
