package main

import (
	"fmt"
	"intern/helper"
)

// melakukan initialisasi
// go mod init intern

func main() {
	conferenceName := "Go Conference"
	conferenceTickets := 50

	fmt.Printf("wellcome to %v booking application\n", conferenceName)

	// print all total remain ticket
	// memanggil package helper
	fmt.Println("total: ", helper.Scan(conferenceTickets, "no"))
}
