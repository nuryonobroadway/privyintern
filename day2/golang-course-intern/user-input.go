package main

import "fmt"

func main() {
	// definisi awal
	// bisa juga memakai var username string
	var username string
	// input = adnan

	fmt.Print("enter username: ")

	// scan begin here,
	fmt.Scan(&username)

	// output = adnan
	fmt.Println(username)

}
