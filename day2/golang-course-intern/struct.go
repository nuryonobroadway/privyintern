package main

import (
	"fmt"
	"math/rand"
)

// deklarasi struct secara sendiri sendiri
type Sample struct {
	number int
}

// deklarasi yang bisa memuat berbagai type
type (
	AnotherSample struct {
		number int
	}

	ExampleInterface interface {
		rescue(int) int
	}
)

func main() {
	// pemanggilan sample
	sample := Sample{
		number: rand.Int(),
	}

	fmt.Println(sample.rescue(rand.Int()))

	// pengambilan identifikasi sample
	// menggunakan kontrak dari
	// Example interface
	anotherexample := NewSample(rand.Int())
	fmt.Println(anotherexample.rescue(rand.Int()))
}

// menggunakan kontrak pada interface dengan
// return berupa struct
func NewSample(number int) ExampleInterface {
	return Sample{
		number: number,
	}
}

// method setelah di kontrak oleh interface
// maupun function yang mengikuti struct
func (s Sample) rescue(target int) int {
	return s.number - target
}
