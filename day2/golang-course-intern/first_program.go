// define package
// karena package nya tidak menggunakan sub folder maka
// definisi package menggunakan main

package main

import "fmt"

// fungsi yang akan dieksekusi pertama kali
func main() {

	// print pesan hello from adnan
	fmt.Println("hello from adnan")
}
