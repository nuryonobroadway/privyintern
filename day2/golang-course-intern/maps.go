package main

import (
	"fmt"
	"math/rand"
)

func main() {
	// usual declaration
	number := make(map[int]bool)

	stock := 0

	// generate baru dengan random integer
	for stock < 10 {
		sample := rand.Int()

		// mengisi map dengan index dari return
		// rand.int() dan dengan value
		// hasil validasi pada return dari
		// rand.Int()
		number[sample] = validation(sample)
		stock++
	}

	// iterasi map number
	for index, value := range number {
		// cek apakah value true
		if value {
			fmt.Printf("%v is can divide by 5\n", index)
		}
	}

	// other declaration
	number = map[int]bool{}
}

// validation jika number bisa di modulo 5
// maka akan return true
// sebaliknya akan return false
func validation(number int) bool {
	return number%5 == 0
}
