package main

import "fmt"

func main() {
	// type storing variable yang bisa diganti
	var firstName = "adnan"

	// type yang tidak bisa diganti
	const lastName = "nuryono"
	firstName = "adnan"
	// print firstname
	fmt.Println("hello ", firstName)

	// print dengan format sesuai dengan format %s atau %v
	// yang berarti format string sedangkan %v => variable
	// yang bisa menampung segala macam type
	fmt.Printf("Fullname: %s %s\n", firstName, lastName)
}
