package main

import (
	"fmt"
	"math/rand"
)

func main() {
	// function dengan parameter dan function dengan return
	// function dengan return dijadikan sebagai paramater
	fmt.Println("with parameter and with return")
	withParameter(withReturn())

	// function yang tidak menyertakan paramter dan return
	fmt.Println("with no return")
	noReturn()

	// function yang menyertakan paramater dan return
	// dengan parameter dari function dengan return
	fmt.Println("with parameter and return")
	fmt.Println(withParameterAndReturn(withReturn()))
}

// menggunakan rand.Int untuk generate random number
func noReturn() {
	fmt.Println(rand.Int())
}

func withReturn() int {
	return rand.Int()
}

func withParameter(number int) {
	fmt.Println(rand.Int())
}

func withParameterAndReturn(number int) int {
	return number - rand.Int()
}
