package main

import "fmt"

func main() {

	// ada beberapa cara deklarasi array dalam golang
	// yang paling umum ada 3
	array1 := []string{}
	var array2 []string

	// beda pengecualian untuk pembuatan array ini,
	// dimana make() adalah membuat array kosong berdasarkan
	// size yang user input atau berikan,
	// jika user memberikan input size 2
	// maka perubahan array nya []string{} tapi dengan bentuk
	// array kosong karena string
	// jika diganti type nya menggunakan int maka seperti ini [0 0]
	array3 := make([]string, 0)

	word := []string{"a", "b", "c", "d", "e", "f"}
	for _, i := range word {
		// menambahkan variable dari i ke array yang telah ada
		array1 = append(array1, i)
		array2 = append(array2, i)
		array3 = append(array3, i)
	}

	// [a b c d e f] [a b c d e f] [a b c d e f]
	fmt.Println(array1, array2, array3)
}
