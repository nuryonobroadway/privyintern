package main

import "fmt"

func main() {
	// deklarasi type interface
	// interface adalah type yang dapat menampung segala aspek apapun
	var value interface{}

	// deklarasi value interface = 0
	value = 0

	// cek type
	switch value.(type) {

	// jika int
	case int:
		fmt.Println("value is int")

	// jika string
	case string:
		fmt.Println("value is string")

	// jika boolean
	case bool:
		fmt.Println("value is boolean")
	default:
		fmt.Println("not supported")
	}

	// switch juga bisa berperan layaknya if else
	number := 10
	switch {
	case number == 10:
		fmt.Println("value is same")
	case number < 10:
		fmt.Println("less than 10")
	case number > 10:
		fmt.Println("more than 10")
	default:
		fmt.Println("error")
	}

	// switch mengecek value pada valid
	valid := false
	switch valid {

	// jika isinya adalah true
	case true:
		fmt.Println("valid")

	// jika isinya adalah false
	case false:
		fmt.Println("not valid")
	default:
		fmt.Println("error")
	}
}
