package main

import "fmt"

func main() {
	number := 1     // angka
	float := 1.2    // float atau angka tidak satuan
	word := "adnan" // string
	boolean := true // boolean true atau false

	fmt.Println(number, float, word, boolean)
}
