package main

func main() {
	// goroutine bisa dipanggila dengan anonymouse function
	// maupun memangill function yang telah ada dengan
	// menggunakan awalan go

	go func() {

	}()

	go sampleRoutine()

	// goroutine akan berjalan di background
}

func sampleRoutine() {

}
