package main

import "fmt"

func main() {
	var value int

	// user input angka lebih dari lima
	fmt.Print("input number more than 5: ")
	fmt.Scan(&value)

	// melakukan validasi satu line dengan return respon
	// adalah boolean
	if err := validation(value, 5); err {
		fmt.Println("value more than 5")
	}
}

func validation(value int, target int) bool {
	// memakai switch
	switch {

	// jika value kurang dari target
	case value > target:

		// maka akan return true
		return true

	default:

		// jika tidak maka akan return false
		return false
	}
}
