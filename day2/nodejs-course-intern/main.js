/**
 * Javascript DOM
*/
console.log(window);

alert(1);

//single element
const form = document.getElementById('my-form');
console.log(form);

console.log(document.querySelector('.container'));

// Multiple element
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByClassName('item'));
console.log(document.getElementsByTagName('li'));

const items = document.querySelectorAll('.item');

items.forEach((item) => console.log(item));

const ul = document.querySelector('.items');
// ul.remove();
//ul.lastElementChild.remove();

ul.firstElementChild.textContent = 'Hello';
ul.children[1].innerText = 'Adnan';
ul.lastElementChild.innerHTML = '<h1>Hello</h1>'

const btn = document.querySelector('.btn');
btn.style.background = 'red'

const btnTwo = document.querySelector('.btn');

// Two.addEventListener('click', (e) => {
// .preventDefault();
// onsole.log('click');
// onsole.log(e.target);
// onsole.log(e.target.id);
// ocument.querySelector('#my-form')
// style.background = '#ccc'
// ocument.querySelector('body').classList.add('bg-dark');
// ocument.querySelector('.items')
// lastElementChild.innerHTML = '<h1>Hello</h1>';
// 

// btnTwo.addEventListener('mouseover', (e) => {
//   e.preventDefault();
//   console.log('click');
//   console.log(e.target);
//   console.log(e.target.id);
//   document.querySelector('#my-form')
//   .style.background = '#ccc'
//   document.querySelector('body').classList.add('bg-dark');
//   document.querySelector('.items')
//   .lastElementChild.innerHTML = '<h1>Hello</h1>';
// });

// btnTwo.addEventListener('mouseout', (e) => {
//   e.preventDefault();
//   console.log('click');
//   console.log(e.target);
//   console.log(e.target.id);
//   document.querySelector('#my-form')
//   .style.background = '#ccc'
//   document.querySelector('body').classList.add('bg-dark');
//   document.querySelector('.items')
//   .lastElementChild.innerHTML = '<h1>Hello</h1>';
// });

const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users ');

myForm = document.addEventListener('submit', onsubmit);

function onsubmit(e) {
    e.preventDefault();

    if (nameInput.value == '' || emailInput.value === '') {
        msg.classList.add('error');
        msg.innerHTML = 'Please enter all fields';

        setTimeout(() => msg.remove(), 3000)
    } else {
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(`${nameInput.value} : ${emailInput.value}`));

        userList.appendChild(li);

        // Clear fields
        nameInput.value = '';
        emailInput.value = '';
    }
}