package token

import (
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"github.com/nuryonobroadway/privy-hardcode/util"
)

type Token struct {
	secretKey     string
	tokenDuration time.Duration
}

type UserClaims struct {
	jwt.RegisteredClaims
	ID        int              `json:"id,omitempty"`
	Username  string           `json:"username,omitempty"`
	CreatedAt *jwt.NumericDate `json:"created_at,omitempty"`
}

type RefreshToken struct {
	jwt.RegisteredClaims
	ID int `json:"id"`
}

func NewToken(secretKey string, tokenDuration time.Duration) *Token {
	return &Token{secretKey, tokenDuration}
}

type GenToken struct {
	token *Token
	user  *UserClaims
}

// function to generate a token
func (g *GenToken) GenerateAccessToken() (string, error) {
	claims := UserClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(g.token.tokenDuration)),
		},
		Username:  g.user.Username,
		ID:        g.user.ID,
		CreatedAt: jwt.NewNumericDate(time.Now()),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(g.token.secretKey))
}

func (g *GenToken) GenerateRefreshToken() (string, error) {
	claims := RefreshToken{
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 24)),
		},
		ID: g.user.ID,
	}

	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return refreshToken.SignedString([]byte(g.token.secretKey))

}

func (manager *Token) GenerateToken(user *util.User) (map[string]string, error) {
	claims := &UserClaims{
		ID:       user.ID,
		Username: user.Username,
	}

	g := GenToken{manager, claims}
	t, err := g.GenerateAccessToken()
	if err != nil {
		return nil, err
	}

	rt, err := g.GenerateRefreshToken()
	if err != nil {
		return nil, err
	}

	return map[string]string{
		"access_token":  t,
		"refresh_token": rt,
		"expired_at":    fmt.Sprint(time.Now().Add(manager.tokenDuration).Unix()),
	}, nil
}

// function to verify a token and return a claims
func (manager *Token) Verify(accessToken string) (*UserClaims, error) {
	token, err := jwt.ParseWithClaims(
		accessToken,
		&UserClaims{},
		func(token *jwt.Token) (interface{}, error) {
			_, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok {
				return nil, fmt.Errorf("unexpected token signing method")
			}
			return []byte(manager.secretKey), nil
		},
	)

	if err != nil {
		return nil, fmt.Errorf("invalid token: %w", err)
	}
	claims, ok := token.Claims.(*UserClaims)
	if !ok {
		return nil, fmt.Errorf("invalid token claims")
	}
	return claims, nil
}
