package server

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/db"
)

type loginRequest struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

func (s *server) handlerUserLogin(c *gin.Context) {
	var newUser loginRequest

	if err := c.ShouldBindJSON(&newUser); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	query := fmt.Sprintf("username=eq.%v&status=eq.%v", newUser.Username, db.Login)
	log, err := s.repository.IsUserAlreadyLogin(query)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if log {
		c.JSON(http.StatusForbidden, gin.H{
			"errors": fmt.Errorf("user already login").Error(),
		})
		return
	}

	filter := fmt.Sprintf("username=eq.%v&select=*", newUser.Username)
	user, err := s.repository.GetUserByID(filter)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if decryptPassword(s.config.HexKey, user.Password, newUser.Password) {
		token, err := s.token.GenerateToken(user)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"errors": err.Error(),
			})
			return
		}

		err = s.repository.InsertSession(user, token["access_token"])
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"errors": err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"response": "success",
			"token":    token,
		})
		return
	}

	c.JSON(http.StatusForbidden, gin.H{
		"errors": fmt.Errorf("password not same"),
	})
}
