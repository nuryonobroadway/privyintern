package server

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/token"
)

type updateMeetingRequest struct {
	NewMeetingID string `json:"code,omitempty"`
	Username     string `uri:"username" binding:"required"`
	MeetingID    string `uri:"meeting_id" binding:"required"`
}

func (s *server) handlerMeetingUpdate(c *gin.Context) {
	var newMeeting updateMeetingRequest
	if err := c.ShouldBindJSON(&newMeeting); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	var meetingInfo updateMeetingRequest
	if err := c.ShouldBindUri(&meetingInfo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	payload := c.MustGet(authorizationPayloadKey).(*token.UserClaims)
	if meetingInfo.Username != payload.Username {
		c.JSON(http.StatusNotAcceptable, gin.H{
			"errors": fmt.Errorf("username not same with payload").Error(),
		})
		return
	}

	filter := fmt.Sprintf("username=eq.%v&select=*", payload.Username)
	user, err := s.repository.GetUserByID(filter)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if user.Username != payload.Username {
		c.JSON(http.StatusForbidden, gin.H{
			"errors": fmt.Errorf("username not same").Error(),
		})
		return
	}

	query := fmt.Sprintf("id=eq.%v&select=*", meetingInfo.MeetingID)
	meeting, err := s.repository.GetMeetingByValue(query)
	if len(meeting) == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": fmt.Errorf("meeting not found").Error(),
		})
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	meeting[0].MeetingID = newMeeting.NewMeetingID
	q := fmt.Sprintf("id=eq.%v", meetingInfo.MeetingID)
	if err := s.repository.UpdateMeeting(q, meeting[0]); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	// convert struct to byte

	c.JSON(http.StatusOK, gin.H{
		"response": "success",
		"status":  http.StatusOK,
	})

}
