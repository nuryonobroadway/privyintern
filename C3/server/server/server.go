package server

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/db"
	"github.com/nuryonobroadway/privy-hardcode/token"
	"github.com/nuryonobroadway/privy-hardcode/util"
)

type (
	server struct {
		config     util.Config
		repository db.Repository
		router     *gin.Engine
		token      *token.Token
	}
)

func NewServer(c util.Config, repository db.Repository, token *token.Token) *server {
	return &server{
		config:     c,
		repository: repository,
		router:     gin.Default(),
		token:      token,
	}
}

func (s *server) Route() {
	auth := s.router.Group("/auth")

	// user before auth
	auth.POST("/login", s.handlerUserLogin)
	auth.POST("/register", s.handlerUserRegister)
	auth.POST("/refresh_token", s.handlerRefreshToken)

	m := middleware{s.token, s.repository}
	content := s.router.Group("/v1").Use(m.authMiddleware())

	// user after auth
	content.GET("/logout/:username", s.handlerUserLogout)
	content.GET("/profile/:username", s.handlerUserProfile)
	content.PATCH("/update_password/:username", s.handlerUserUpdatePassword)
	content.GET("/session/:username", s.handlerUserSession)
	content.DELETE("/delete_session/:username/:session_id", s.handlerUserSessionDelete)

	// meeting
	content.POST("/add_meeting/:username", s.handlerMeetingAdd)
	content.POST("/join_meeting/:username", s.handlerMeetingJoin)
	content.PATCH("/update_meeting/:username/:meeting_id", s.handlerMeetingUpdate)
	content.DELETE("/delete_meeting/:username/:meeting_id", s.handlerMeetingDelete)
}

func (s *server) Start() error {
	fmt.Println("server start on 0.0.0.0:9000")
	return s.router.Run("0.0.0.0:9000")
}
