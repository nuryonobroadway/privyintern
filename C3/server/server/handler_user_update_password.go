package server

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/token"
)

type updatePasswordRequest struct {
	OldPassword     string `json:"old_password,omitempty"`
	NewPassword     string `json:"new_password,omitempty"`
	ConfNewPassword string `json:"conf_new_password,omitempty"`
}

type getusername struct {
	Username string `uri:"username" binding:"required"`
}

func (s *server) handlerUserUpdatePassword(c *gin.Context) {
	var password updatePasswordRequest

	if err := c.ShouldBindJSON(&password); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	var username getusername
	if err := c.ShouldBindUri(&username); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	payload := c.MustGet(authorizationPayloadKey).(*token.UserClaims)
	if username.Username != payload.Username {
		c.JSON(http.StatusNotAcceptable, gin.H{
			"errors": fmt.Errorf("username not same with payload").Error(),
		})
		return
	}

	fmt.Println(password)
	if password.NewPassword != password.ConfNewPassword {
		c.JSON(http.StatusNotAcceptable, gin.H{
			"errors": fmt.Errorf("password not same").Error(),
		})
		return
	}

	filter := fmt.Sprintf("id=eq.%v&select=*", payload.ID)
	user, err := s.repository.GetUserByID(filter)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if user.Username != payload.Username {
		c.JSON(http.StatusForbidden, gin.H{
			"errors": fmt.Errorf("username not same").Error(),
		})
		return
	}

	if decryptPassword(s.config.HexKey, user.Password, password.OldPassword) {
		user.Password = encryptPassword(s.config.HexKey, password.NewPassword)

		query := fmt.Sprintf("id=eq.%v", user.ID)
		if err := s.repository.UpdatePassword(query, user); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"errors": err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"response": "success",
			"status":   http.StatusOK,
		})
		return
	}

	c.JSON(http.StatusInternalServerError, gin.H{
		"errors": fmt.Errorf("error when update user password").Error(),
	})
}
