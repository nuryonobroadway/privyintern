package server

import (
	"github.com/nuryonobroadway/privy-hardcode/util"
)

func encryptPassword(key string, password string) string {
	e := util.Encrypt{
		Key: key,
	}
	return e.EncryptText(password)
}

func decryptPassword(key string, decrypt_password, password string) bool {
	e := util.Encrypt{
		Key: key,
	}

	pass := e.DecryptText(decrypt_password)
	return pass == password
}
