package server

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/util"
)

type registerRequest struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

func (s *server) handlerUserRegister(c *gin.Context) {
	var newUser registerRequest

	if err := c.ShouldBindJSON(&newUser); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	user := &util.User{
		Username: newUser.Username,
		Password: encryptPassword(s.config.HexKey, newUser.Password),
		RoleID:   util.Guest,
	}
	if err := s.repository.InsertUser(user); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"response": "success",
		"status":   http.StatusOK,
	})
}
