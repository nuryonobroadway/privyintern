package server

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/token"
)

type userProfileRequest struct {
	Username string `uri:"username" binding:"required"`
}

func (s *server) handlerUserProfile(c *gin.Context) {
	var username userProfileRequest

	if err := c.ShouldBindUri(&username); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	payload := c.MustGet(authorizationPayloadKey).(*token.UserClaims)
	if username.Username != payload.Username {
		c.JSON(http.StatusNotAcceptable, gin.H{
			"errors": fmt.Errorf("username not same with payload").Error(),
		})
		return
	}

	filter := fmt.Sprintf("username=eq.%v&select=*", payload.Username)
	user, err := s.repository.GetUserByID(filter)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if user.Username != payload.Username {
		c.JSON(http.StatusForbidden, gin.H{
			"errors": fmt.Errorf("username not same").Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"response": "success",
		"user":     user,
	})

}
