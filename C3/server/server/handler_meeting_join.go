package server

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/token"
	"github.com/nuryonobroadway/privy-hardcode/util"
)

type joinMeetingRequest struct {
	Code     string `json:"code,omitempty"`
	Username string `uri:"username" binding:"required"`
}

func (s *server) handlerMeetingJoin(c *gin.Context) {
	var code joinMeetingRequest
	if err := c.ShouldBindJSON(&code); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	var username joinMeetingRequest
	if err := c.ShouldBindUri(&username); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	payload := c.MustGet(authorizationPayloadKey).(*token.UserClaims)
	if username.Username != payload.Username {
		c.JSON(http.StatusNotAcceptable, gin.H{
			"errors": fmt.Errorf("username not same with payload").Error(),
		})
		return
	}

	filter := fmt.Sprintf("username=eq.%v&select=*", payload.Username)
	user, err := s.repository.GetUserByID(filter)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if user.Username != payload.Username {
		c.JSON(http.StatusForbidden, gin.H{
			"errors": fmt.Errorf("username not same").Error(),
		})
		return
	}

	var meeting *util.Meeting

	e := util.Encrypt{Key: s.config.LinkKey}
	result := e.DecodeByte(e.DecryptText(code.Code))
	err = json.Unmarshal(result, &meeting)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if err := s.repository.InsertAttend(user, meeting.ID, util.Viewer); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	// convert struct to byte

	c.JSON(http.StatusOK, gin.H{
		"response": "success",
		"meeting":  meeting,
	})

}
