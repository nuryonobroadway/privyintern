package server

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/db"
	"github.com/nuryonobroadway/privy-hardcode/token"
)

type logoutRequest struct {
	Username string `uri:"username" binding:"required"`
}

func (s *server) handlerUserLogout(c *gin.Context) {
	var logout logoutRequest

	if err := c.ShouldBindUri(&logout); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	payload := c.MustGet(authorizationPayloadKey).(*token.UserClaims)
	if logout.Username != payload.Username {
		c.JSON(http.StatusNotAcceptable, gin.H{
			"errors": fmt.Errorf("username not same with payload").Error(),
		})
		return
	}

	query := fmt.Sprintf("user_id=eq.%v&username=eq.%v&status=eq.%v", payload.ID, logout.Username, db.Login)
	log, err := s.repository.IsUserAlreadyLogin(query)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if log {
		filter := fmt.Sprintf("username=eq.%v&user_id=eq.%v&status=eq.%v", payload.Username, payload.ID, db.Login)
		session, err := s.repository.GetSessionByValue(filter)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"errors": err.Error(),
			})
			return
		}

		session[0].Status = db.Logout
		session[0].LogoutAt = time.Now()
		if err := s.repository.UpdateSession(filter, session[0]); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"errors": err.Error(),
			})
			return
		}

		c.JSON(http.StatusAccepted, gin.H{
			"response": "success",
			"status":   http.StatusAccepted,
		})
		return

	}

	c.JSON(http.StatusForbidden, gin.H{
		"errors": fmt.Errorf("user already logout"),
	})
}
