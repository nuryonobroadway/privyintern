package server

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type refreshTokenRequest struct {
	RefreshToken string `json:"refresh_token,omitempty"`
}

func (s *server) handlerRefreshToken(c *gin.Context) {
	var refreshToken refreshTokenRequest

	if err := c.ShouldBindJSON(&refreshToken); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	refreshPayload, err := s.token.Verify(refreshToken.RefreshToken)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"errors": err.Error(),
		})
		return
	}

	filter := fmt.Sprintf("id=eq.%v&select=*", refreshPayload.ID)
	user, err := s.repository.GetUserByID(filter)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	fmt.Println(refreshPayload)
	if user.ID != refreshPayload.ID {
		c.JSON(http.StatusForbidden, gin.H{
			"errors": err.Error(),
		})
		return
	}

	token, err := s.token.GenerateToken(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"response": "success",
		"token":    token,
	})

}
