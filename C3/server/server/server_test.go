package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/nuryonobroadway/privy-hardcode/db"
	mockdb "github.com/nuryonobroadway/privy-hardcode/db/mock"
	"github.com/nuryonobroadway/privy-hardcode/token"
	"github.com/nuryonobroadway/privy-hardcode/util"
	"github.com/stretchr/testify/require"
)

func TestMain(m *testing.M) {
	gin.SetMode(gin.TestMode)

	os.Exit(m.Run())
}

func newTestServer(t *testing.T, store db.Repository) *server {
	token := token.NewToken("21BBE3E1873124237257E5E67651257F", 1*time.Minute)
	config, err := util.LoadConfig("./..")
	require.NoError(t, err)

	server := NewServer(config, store, token)
	server.Route()
	return server
}

func addAuthorization(
	t *testing.T,
	request *http.Request,
	token *token.Token,
	authorizationType string,
	user *util.User,
) {
	gentoken, err := token.GenerateToken(user)
	require.NoError(t, err)

	authorizationHeader := fmt.Sprintf("%s %s", authorizationType, gentoken["access_token"])
	request.Header.Set(authorizationHeaderKey, authorizationHeader)
}

func TestRegisterUserAPI(t *testing.T) {
	testCases := []struct {
		name          string
		body          gin.H
		buildStubs    func(store *mockdb.MockRepository)
		checkResponse func(recoder *httptest.ResponseRecorder)
	}{
		{
			name: "OK",
			body: gin.H{
				"username": "test",
				"password": "test",
			},
			buildStubs: func(store *mockdb.MockRepository) {
				store.EXPECT().InsertUser(gomock.Any()).
					Times(1).
					Return(nil)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusOK, recorder.Code)
			},
		}, {
			name: "Username Already Taken",
			body: gin.H{
				"username": "test",
				"password": "test",
			},
			buildStubs: func(store *mockdb.MockRepository) {
				store.EXPECT().InsertUser(gomock.All()).
					Times(1).
					Return(fmt.Errorf("username already available"))
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusInternalServerError, recorder.Code)
			},
		}, {
			name: "Bad Request",
			body: gin.H{
				"username": 1,
			},
			buildStubs: func(store *mockdb.MockRepository) {
				store.EXPECT().InsertUser(gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusBadRequest, recorder.Code)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mockdb.NewMockRepository(ctrl)
			tc.buildStubs(store)

			server := newTestServer(t, store)
			recorder := httptest.NewRecorder()

			data, err := json.Marshal(tc.body)
			require.NoError(t, err)

			url := "/auth/register"
			request, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
			require.NoError(t, err)

			server.router.ServeHTTP(recorder, request)
			tc.checkResponse(recorder)
		})
	}
}

func TestLoginUserAPI(t *testing.T) {
	user := &util.User{
		ID:        1,
		Username:  "test",
		Password:  encryptPassword("21BBE3E1873124237257E5E67651257F", "test"),
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}

	testCases := []struct {
		name          string
		body          gin.H
		buildStubs    func(store *mockdb.MockRepository)
		checkResponse func(recoder *httptest.ResponseRecorder)
	}{
		{
			name: "OK",
			body: gin.H{
				"username": "test",
				"password": "test",
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("username=eq.%v&select=*", user.Username)
				store.EXPECT().GetUserByID(filter).
					Times(1).
					Return(user, nil)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusOK, recorder.Code)
				fmt.Println(recorder.Body)
			},
		}, {
			name: "Wrong Password",
			body: gin.H{
				"username": "test",
				"password": "wrong",
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("username=eq.%v&select=*", user.Username)

				store.EXPECT().GetUserByID(gomock.Eq(filter)).
					Times(1).
					Return(user, nil)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusForbidden, recorder.Code)
			},
		}, {
			name: "Bad Request",
			body: gin.H{
				"username": 1,
			},
			buildStubs: func(store *mockdb.MockRepository) {
				store.EXPECT().GetUserByID(gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusBadRequest, recorder.Code)
			},
		},
		{
			name: "Not Found",
			body: gin.H{
				"username": "test",
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("username=eq.%v&select=*", "test")

				store.EXPECT().GetUserByID(gomock.Eq(filter)).
					Times(1).
					Return(nil, fmt.Errorf("user not found"))
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusNotFound, recorder.Code)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mockdb.NewMockRepository(ctrl)
			tc.buildStubs(store)

			server := newTestServer(t, store)
			recorder := httptest.NewRecorder()

			data, err := json.Marshal(tc.body)
			require.NoError(t, err)

			url := "/auth/login"
			request, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
			require.NoError(t, err)

			server.router.ServeHTTP(recorder, request)
			tc.checkResponse(recorder)
		})
	}
}

func TestRefreshTokenUserAPI(t *testing.T) {
	user := &util.User{
		ID:        1,
		Username:  "test",
		Password:  encryptPassword("21BBE3E1873124237257E5E67651257F", "test"),
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}

	refreshToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzE2OTY2OTksImlkIjoxfQ.qIVgZRDjsbZJIjn_7AciSuBmbyPkFzg1cLSqHB6GnzA"

	testCases := []struct {
		name          string
		body          gin.H
		buildStubs    func(store *mockdb.MockRepository)
		checkResponse func(recoder *httptest.ResponseRecorder)
	}{
		{
			name: "OK",
			body: gin.H{
				"refresh_token": refreshToken,
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("id=eq.%v&select=*", user.ID)
				store.EXPECT().GetUserByID(filter).
					Times(1).
					Return(user, nil)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusOK, recorder.Code)
				fmt.Println(recorder.Body)
			},
		},
		{
			name: "Bad Request",
			body: gin.H{
				"refresh_token": 1,
			},
			buildStubs: func(store *mockdb.MockRepository) {
				store.EXPECT().GetUserByID(gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusBadRequest, recorder.Code)
			},
		},
		{
			name: "Unautorized",
			body: gin.H{
				"refresh_token": "wrong",
			},
			buildStubs: func(store *mockdb.MockRepository) {
				store.EXPECT().GetUserByID(gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusUnauthorized, recorder.Code)
			},
		},
		{
			name: "NotFound",
			body: gin.H{
				"refresh_token": refreshToken,
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("id=eq.%v&select=*", 1)
				store.EXPECT().GetUserByID(gomock.Eq(filter)).
					Times(1).
					Return(nil, fmt.Errorf("user not found"))
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusNotFound, recorder.Code)
			},
		},
		{
			name: "Forbidden",
			body: gin.H{
				"refresh_token": refreshToken,
			},
			buildStubs: func(store *mockdb.MockRepository) {
				user.ID = 2
				filter := fmt.Sprintf("id=eq.%v&select=*", 1)
				store.EXPECT().GetUserByID(gomock.Eq(filter)).
					Times(1).
					Return(user, nil)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusForbidden, recorder.Code)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mockdb.NewMockRepository(ctrl)
			tc.buildStubs(store)

			server := newTestServer(t, store)
			recorder := httptest.NewRecorder()

			data, err := json.Marshal(tc.body)
			require.NoError(t, err)

			url := "/auth/refresh_token"
			request, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
			require.NoError(t, err)

			server.router.ServeHTTP(recorder, request)
			tc.checkResponse(recorder)
		})
	}
}

func TestUserUpdatePasswordAPI(t *testing.T) {
	user := &util.User{
		ID:        1,
		Username:  "test",
		Password:  encryptPassword("21BBE3E1873124237257E5E67651257F", "test"),
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}

	testCases := []struct {
		name          string
		url           string
		body          gin.H
		setupAuth     func(t *testing.T, request *http.Request, token *token.Token)
		buildStubs    func(store *mockdb.MockRepository)
		checkResponse func(recoder *httptest.ResponseRecorder)
	}{
		{
			name: "OK",
			url:  fmt.Sprintf("/v1/update_password/%v", user.Username),
			body: gin.H{
				"old_password":      "test",
				"new_password":      "123",
				"conf_new_password": "123",
			},
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("id=eq.%v&select=*", user.ID)
				store.EXPECT().GetUserByID(filter).
					Times(1).
					Return(user, nil)

				query := fmt.Sprintf("id=eq.%v", user.ID)
				store.EXPECT().UpdatePassword(query, user).
					Times(1).
					Return(nil)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusOK, recorder.Code)
				fmt.Println(recorder.Body)
			},
		},
		{
			name: "Bad Request",
			url:  fmt.Sprintf("/v1/update_password/%v", user.Username),
			body: gin.H{
				"old_password": 1,
			},
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				store.EXPECT().GetUserByID(gomock.Any()).
					Times(0)

				store.EXPECT().UpdatePassword(gomock.Any(), gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusBadRequest, recorder.Code)
			},
		},
		{
			name: "Not Acceptable",
			url:  fmt.Sprintf("/v1/update_password/%v", user.Username),
			body: gin.H{
				"old_password":      "test",
				"new_password":      "123",
				"conf_new_password": "wrong",
			},
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				store.EXPECT().GetUserByID(gomock.Any()).
					Times(0)

				store.EXPECT().UpdatePassword(gomock.Any(), gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusNotAcceptable, recorder.Code)
			},
		},
		{
			name: "Not Found",
			url:  fmt.Sprintf("/v1/update_password/%v", user.Username),
			body: gin.H{
				"old_password":      "test",
				"new_password":      "123",
				"conf_new_password": "123",
			},
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("id=eq.%v&select=*", user.ID)
				store.EXPECT().GetUserByID(filter).
					Times(1).
					Return(nil, fmt.Errorf("user not found"))

				store.EXPECT().UpdatePassword(gomock.Any(), gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusNotFound, recorder.Code)
			},
		},
		{
			name: "Forbidden",
			url:  fmt.Sprintf("/v1/update_password/%v", user.Username),
			body: gin.H{
				"old_password":      "test",
				"new_password":      "123",
				"conf_new_password": "123",
			},
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("id=eq.%v&select=*", user.ID)
				wrongUser := &util.User{
					ID:        1,
					Username:  "wrong",
					Password:  encryptPassword("21BBE3E1873124237257E5E67651257F", "test"),
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
				}

				store.EXPECT().GetUserByID(filter).
					Times(1).
					Return(wrongUser, nil)

				store.EXPECT().UpdatePassword(gomock.Any(), gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusForbidden, recorder.Code)
			},
		},
		{
			name: "Username not same with payload",
			url:  "/v1/update_password/wrong",
			body: gin.H{
				"old_password":      "test",
				"new_password":      "123",
				"conf_new_password": "123",
			},
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				store.EXPECT().GetUserByID(gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusNotAcceptable, recorder.Code)
			},
		},
		{
			name: "Password Not Match",
			url:  fmt.Sprintf("/v1/update_password/%v", user.Username),
			body: gin.H{
				"old_password":      "test",
				"new_password":      "123",
				"conf_new_password": "123",
			},
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("id=eq.%v&select=*", user.ID)
				wrongUser := &util.User{
					ID:        1,
					Username:  "test",
					Password:  encryptPassword("21BBE3E1873124237257E5E67651257F", "wrong"),
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
				}

				store.EXPECT().GetUserByID(filter).
					Times(1).
					Return(wrongUser, nil)

				store.EXPECT().UpdatePassword(gomock.Any(), gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusInternalServerError, recorder.Code)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mockdb.NewMockRepository(ctrl)
			tc.buildStubs(store)

			server := newTestServer(t, store)
			recorder := httptest.NewRecorder()

			data, err := json.Marshal(tc.body)
			require.NoError(t, err)

			request, err := http.NewRequest(http.MethodPatch, tc.url, bytes.NewReader(data))
			require.NoError(t, err)

			tc.setupAuth(t, request, server.token)
			server.router.ServeHTTP(recorder, request)
			tc.checkResponse(recorder)
		})
	}
}

func TestUserProfile(t *testing.T) {
	user := &util.User{
		ID:        1,
		Username:  "test",
		Password:  encryptPassword("21BBE3E1873124237257E5E67651257F", "test"),
		CreatedAt: time.Time{},
		UpdatedAt: time.Time{},
	}

	testCases := []struct {
		name          string
		url           string
		setupAuth     func(t *testing.T, request *http.Request, token *token.Token)
		buildStubs    func(store *mockdb.MockRepository)
		checkResponse func(recoder *httptest.ResponseRecorder)
	}{
		{
			name: "OK",
			url:  fmt.Sprintf("/v1/profile/%v", user.Username),
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("username=eq.%v&select=*", user.Username)
				store.EXPECT().GetUserByID(filter).
					Times(1).
					Return(user, nil)

			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusOK, recorder.Code)
				fmt.Println(recorder.Body)
			},
		},
		{
			name: "Not Acceptable",
			url:  "/v1/profile/wrong",
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				store.EXPECT().GetUserByID(gomock.Any()).
					Times(0)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusNotAcceptable, recorder.Code)
			},
		},
		{
			name: "Not Found",
			url:  fmt.Sprintf("/v1/profile/%v", user.Username),
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				filter := fmt.Sprintf("username=eq.%v&select=*", user.Username)
				store.EXPECT().GetUserByID(filter).
					Times(1).
					Return(nil, fmt.Errorf("user not found"))
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusNotFound, recorder.Code)
			},
		},
		{
			name: "Forbidden",
			url:  fmt.Sprintf("/v1/profile/%v", user.Username),
			setupAuth: func(t *testing.T, request *http.Request, token *token.Token) {
				addAuthorization(t, request, token, authorizationTypeBearer, user)
			},
			buildStubs: func(store *mockdb.MockRepository) {
				wrongUser := &util.User{
					ID:        1,
					Username:  "wrong",
					Password:  encryptPassword("21BBE3E1873124237257E5E67651257F", "test"),
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
				}

				filter := fmt.Sprintf("username=eq.%v&select=*", user.Username)
				store.EXPECT().GetUserByID(filter).
					Times(1).
					Return(wrongUser, nil)
			},
			checkResponse: func(recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusForbidden, recorder.Code)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mockdb.NewMockRepository(ctrl)
			tc.buildStubs(store)

			server := newTestServer(t, store)
			recorder := httptest.NewRecorder()

			request, err := http.NewRequest(http.MethodGet, tc.url, nil)
			require.NoError(t, err)

			tc.setupAuth(t, request, server.token)
			server.router.ServeHTTP(recorder, request)
			tc.checkResponse(recorder)
		})
	}
}
