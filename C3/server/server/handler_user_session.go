package server

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/token"
)

type sessionRequest struct {
	Username string `uri:"username" binding:"required"`
}

func (s *server) handlerUserSession(c *gin.Context) {
	var session sessionRequest

	if err := c.ShouldBindUri(&session); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	payload := c.MustGet(authorizationPayloadKey).(*token.UserClaims)
	if session.Username != payload.Username {
		c.JSON(http.StatusNotAcceptable, gin.H{
			"errors": fmt.Errorf("username not same with payload").Error(),
		})
		return
	}

	filter := fmt.Sprintf("username=eq.%v&user_id=eq.%v", payload.Username, payload.ID)
	sessions, err := s.repository.GetSessionByValue(filter)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"response": "success",
		"session":  sessions,
	})
}
