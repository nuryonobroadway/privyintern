package server

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/token"
	"github.com/nuryonobroadway/privy-hardcode/util"
)

type addMeetingRequest struct {
	MeetingID string `json:"meeting_id,omitempty"`
	Username  string `uri:"username" binding:"required"`
}

func (s *server) handlerMeetingAdd(c *gin.Context) {
	var meetingID addMeetingRequest
	if err := c.ShouldBindJSON(&meetingID); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	var username addMeetingRequest
	if err := c.ShouldBindUri(&username); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	payload := c.MustGet(authorizationPayloadKey).(*token.UserClaims)
	if username.Username != payload.Username {
		c.JSON(http.StatusNotAcceptable, gin.H{
			"errors": fmt.Errorf("username not same with payload").Error(),
		})
		return
	}

	filter := fmt.Sprintf("username=eq.%v&select=*", payload.Username)
	user, err := s.repository.GetUserByID(filter)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if user.Username != payload.Username {
		c.JSON(http.StatusForbidden, gin.H{
			"errors": fmt.Errorf("username not same").Error(),
		})
		return
	}

	meeting, err := s.repository.InsertMeeting(meetingID.MeetingID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if err := s.repository.InsertAttend(user, meeting.ID, util.Host); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	// convert struct to byte
	data, err := json.Marshal(&meeting)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	// generate link
	e := util.Encrypt{Key: s.config.LinkKey}
	code := e.EncryptText(e.EncodeByte(data))

	c.JSON(http.StatusOK, gin.H{
		"response": "success",
		"code":    code,
	})

}
