package server

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/db"
	"github.com/nuryonobroadway/privy-hardcode/token"
	"github.com/nuryonobroadway/privy-hardcode/util"
)

type deleteMeetingRequest struct {
	Username  string `uri:"username" binding:"required"`
	MeetingID string `uri:"meeting_id" binding:"required"`
}

func (s *server) handlerMeetingDelete(c *gin.Context) {
	var meetingInfo deleteMeetingRequest
	if err := c.ShouldBindUri(&meetingInfo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err.Error(),
		})
		return
	}

	payload := c.MustGet(authorizationPayloadKey).(*token.UserClaims)
	if meetingInfo.Username != payload.Username {
		c.JSON(http.StatusNotAcceptable, gin.H{
			"errors": fmt.Errorf("username not same with payload").Error(),
		})
		return
	}

	filter := fmt.Sprintf("username=eq.%v&select=*", payload.Username)
	user, err := s.repository.GetUserByID(filter)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if user.Username != payload.Username {
		c.JSON(http.StatusForbidden, gin.H{
			"errors": fmt.Errorf("username not same").Error(),
		})
		return
	}

	query := fmt.Sprintf("id=eq.%v&select=*", meetingInfo.MeetingID)
	meeting, err := s.repository.GetMeetingByValue(query)
	if len(meeting) == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"errors": fmt.Errorf("meeting not found").Error(),
		})
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	q := fmt.Sprintf("user_id=eq.%v&meeting_id=eq.%v", user.ID, meetingInfo.MeetingID)
	attend, err := s.repository.GetAttendByValue(q)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	if attend[0].RoleID == util.Host {
		meeting := fmt.Sprintf("id=eq.%v", meetingInfo.MeetingID)
		if err := s.repository.DeleteMeetingAttend(meeting, db.Meeting_table); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"errors": err.Error(),
			})
			return
		}
	}
	// convert struct to byte

	a := fmt.Sprintf("id=eq.%v&user_id=eq.%v&meeting_id=eq.%v&", attend[0].ID, user.ID, meetingInfo.MeetingID)
	if err := s.repository.DeleteMeetingAttend(a, db.Attend_table); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"errors": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"response": "success",
		"status":   http.StatusOK,
	})

}
