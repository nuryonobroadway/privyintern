package server

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/db"
	"github.com/nuryonobroadway/privy-hardcode/token"
)

const (
	authorizationHeaderKey  = "authorization"
	authorizationTypeBearer = "bearer"
	authorizationPayloadKey = "authorization_payload"
)

type middleware struct {
	token      *token.Token
	repository db.Repository
}

func (m *middleware) authMiddleware() gin.HandlerFunc {
	return func(g *gin.Context) {
		authorizationHeader := g.GetHeader(authorizationHeaderKey)

		if len(authorizationHeader) == 0 {
			g.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": "authorization header is not provided",
			})
			return
		}

		fields := strings.Fields(authorizationHeader)
		if len(fields) < 2 {
			err := errors.New("invalid authorization header format")
			g.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": err.Error(),
			})
			return
		}

		authorizationType := strings.ToLower(fields[0])
		if authorizationType != authorizationTypeBearer {
			err := fmt.Errorf("unsupported authorization type %s", authorizationType)
			g.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": err.Error(),
			})
			return
		}

		accessToken := fields[1]
		payload, err := m.token.Verify(accessToken)
		if err != nil {
			filter := fmt.Sprintf("token=eq.%v", accessToken)
			session, err := m.repository.GetSessionByValue(filter)
			if err != nil {
				g.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
					"error": err.Error(),
				})
				return
			}

			if session[0].Status == db.Login {
				session[0].Status = db.Logout
				session[0].LogoutAt = time.Now()
				if err := m.repository.UpdateSession(filter, session[0]); err != nil {
					g.JSON(http.StatusInternalServerError, gin.H{
						"errors": err.Error(),
					})
					return
				}
			}

			g.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": err.Error(),
			})
			return
		}

		// set if jwt already expired or user already logout
		filter := fmt.Sprintf("username=eq.%v&user_id=eq.%v&status=eq.%v&token=eq.%v", payload.Username, payload.ID, db.Logout, accessToken)
		session, err := m.repository.GetSessionByValue(filter)
		if err != nil {
			g.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": err.Error(),
			})
			return
		}

		if len(session) != 0 {
			g.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": fmt.Errorf("user already logout").Error(),
			})
			return
		}

		g.Set(authorizationPayloadKey, payload)
		g.Next()
	}
}
