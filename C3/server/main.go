package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/nuryonobroadway/privy-hardcode/initiate"
)

func init() {
	gin.SetMode(gin.DebugMode)
}

func main() {
	if err := initiate.Initiate(); err != nil {
		log.Fatal(err)
	}
}
