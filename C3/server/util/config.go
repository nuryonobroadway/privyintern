package util

import (
	"log"

	"github.com/spf13/viper"
)

type Config struct {
	DatabaseUri   string `mapstructure:"DATABASE_PASSWORD"`
	ProjectURL    string `mapstructure:"PROJECT_URL"`
	ApiKey        string `mapstructure:"API_KEY"`
	ServiceRole   string `mapstructure:"SERVICE_ROLE"`
	HexKey        string `mapstructure:"HEX_KEY"`
	LinkKey       string `mapstructure:"LINK_KEY"`
	TokenSecret   string `mapstructure:"TOKEN_SECRET"`
	TokenDuration int    `mapstructure:"TOKEN_DURATION"`
}

func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		log.Println(err)
		return
	}

	err = viper.Unmarshal(&config)
	return
}
