package util

import "time"

const (
	Sharing = "sharing"
)

type (
	User struct {
		ID        int       `json:"id,omitempty"`
		Username  string    `json:"username,omitempty"`
		Password  string    `json:"password,omitempty"`
		RoleID    int       `json:"role_id,omitempty"`
		CreatedAt time.Time `json:"created_at,omitempty"`
		UpdatedAt time.Time `json:"updated_at,omitempty"`
	}

	Session struct {
		ID        int       `json:"id,omitempty"`
		UserID    int       `json:"user_id,omitempty"`
		Username  string    `json:"username,omitempty"`
		Status    string    `json:"status,omitempty"`
		LoginAt   time.Time `json:"login_at,omitempty"`
		LogoutAt  time.Time `json:"logout_at,omitempty"`
		Token     string    `json:"token,omitempty"`
		CreatedAt time.Time `json:"created_at,omitempty"`
	}

	Meeting struct {
		ID        int       `json:"id,omitempty"`
		MeetingID string    `json:"meeting_id,omitempty"`
		CreatedAt time.Time `json:"created_at,omitempty"`
	}

	Attend struct {
		ID        int       `json:"id,omitempty"`
		UserID    int       `json:"user_id,omitempty"`
		MeetingID int       `json:"meeting_id,omitempty"`
		RoleID    int       `json:"role_id,omitempty"`
		CreatedAt time.Time `json:"created_at,omitempty"`
	}

	GeneratedMeeting struct {
		ID        int    `json:"id,omitempty"`
		MeetingID string `json:"meeting_id,omitempty"`
		Status    string `json:"status,omitempty"`
	}
)
