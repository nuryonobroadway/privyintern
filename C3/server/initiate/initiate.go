package initiate

import (
	"time"

	"github.com/nuryonobroadway/privy-hardcode/db"
	"github.com/nuryonobroadway/privy-hardcode/server"
	"github.com/nuryonobroadway/privy-hardcode/token"
	"github.com/nuryonobroadway/privy-hardcode/util"
)

type PrepareDev struct {
	config     util.Config
	token      *token.Token
	repository db.Repository
}

func Initiate() error {
	p := &PrepareDev{}
	var err error

	p.config, err = p.loadConfig()
	if err != nil {
		return err
	}

	p.token = p.loadToken()
	p.repository = p.loadRepository()

	return p.startRest()
}

func (p *PrepareDev) loadConfig() (util.Config, error) {
	return util.LoadConfig("./")
}

func (p *PrepareDev) loadToken() *token.Token {
	return token.NewToken(p.config.TokenSecret, time.Duration(p.config.TokenDuration*int(time.Minute)))
}

func (p *PrepareDev) loadRepository() db.Repository {
	return db.NewInstance(p.config)
}

func (p *PrepareDev) startRest() error {
	server := server.NewServer(p.config, p.repository, p.token)
	server.Route()

	return server.Start()
}
