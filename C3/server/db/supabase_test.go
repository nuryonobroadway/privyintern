package db

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/nuryonobroadway/privy-hardcode/util"
	"github.com/stretchr/testify/require"
)

func initiate() (Repository, error) {
	rand.Seed(time.Now().UnixNano())
	config, err := util.LoadConfig("../")
	if err != nil {
		return nil, err
	}

	return NewInstance(config), nil
}

func TestGetData(t *testing.T) {
	rep, err := initiate()
	require.NoError(t, err)

	filter := "select=*"
	user, err := rep.GetUser(filter)
	require.NoError(t, err)

	fmt.Println(user)
}

func TestGetDataByID(t *testing.T) {
	rep, err := initiate()
	require.NoError(t, err)

	id := 5311255940668274180
	filter := fmt.Sprintf("id=eq.%v&select=*", id)
	user, err := rep.GetUserByID(filter)
	require.NoError(t, err)

	fmt.Println(user)
}

func TestPostData(t *testing.T) {
	rep, err := initiate()
	require.NoError(t, err)

	newUser := &util.User{
		ID:        rand.Int(),
		Username:  "nuryono",
		Password:  "123",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	err = rep.InsertUser(newUser)
	require.NoError(t, err)

}

func TestUpdateData(t *testing.T) {
	rep, err := initiate()
	require.NoError(t, err)
	target := 5311255940668274180

	newUser := &util.User{
		ID:       target,
		Password: "test",
	}

	column := "id"

	filter := fmt.Sprintf("%v=eq.%v", column, target)
	err = rep.UpdatePassword(filter, newUser)
	require.NoError(t, err)

}

func TestDeleteData(t *testing.T) {
	rep, err := initiate()
	require.NoError(t, err)
	target := 5311255940668274180

	column := "id"

	filter := fmt.Sprintf("%v=eq.%v", column, target)
	err = rep.DeleteUser(filter, target)
	require.NoError(t, err)

}
