package db

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"time"

	"github.com/nuryonobroadway/privy-hardcode/util"
)

const (
	Login  = "login"
	Logout = "logout"
)

func (q *repository) InsertSession(user *util.User, token string) error {
	session := &util.Session{
		ID:        rand.Int(),
		UserID:    user.ID,
		Username:  user.Username,
		Status:    Login,
		LoginAt:   time.Now(),
		Token:     token,
		CreatedAt: time.Now(),
	}

	req, err := q.newRequest(http.MethodPost, Session_table, "", session)
	if err != nil {
		return err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil

}

func (q *repository) GetSessionByValue(query string) ([]*util.Session, error) {
	req, err := q.newRequest(http.MethodGet, Session_table, query, nil)
	if err != nil {
		return nil, err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	session := []*util.Session{}
	err = json.NewDecoder(resp.Body).Decode(&session)
	if err != nil {
		return nil, err
	}

	return session, nil
}

func (q *repository) IsUserAlreadyLogin(query string) (bool, error) {
	session, err := q.GetSessionByValue(query)
	if err != nil {
		return false, err
	}

	return len(session) != 0, nil
}

func (q *repository) UpdateSession(query string, session *util.Session) error {
	req, err := q.newRequest(http.MethodPatch, Session_table, query, session)
	if err != nil {
		return err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func (q *repository) DeleteSession(query string) error {
	req, err := q.newRequest(http.MethodDelete, Session_table, query, nil)
	if err != nil {
		return err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil

}
