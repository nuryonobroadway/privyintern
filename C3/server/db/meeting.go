package db

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"time"

	"github.com/nuryonobroadway/privy-hardcode/util"
)

func (q *repository) GetMeetingByValue(query string) ([]*util.Meeting, error) {
	req, err := q.newRequest(http.MethodGet, Meeting_table, query, nil)
	if err != nil {
		return nil, err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	meeting := []*util.Meeting{}
	err = json.NewDecoder(resp.Body).Decode(&meeting)
	if err != nil {
		return nil, err
	}

	return meeting, nil
}

func (q *repository) InsertMeeting(meetingID string) (*util.Meeting, error) {
	meeting := &util.Meeting{
		ID:        rand.Int(),
		MeetingID: meetingID,
		CreatedAt: time.Time{},
	}

	req, err := q.newRequest(http.MethodPost, Meeting_table, "", meeting)
	if err != nil {
		return nil, err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return meeting, nil

}

func (q *repository) GetAttendByValue(query string) ([]*util.Attend, error) {
	req, err := q.newRequest(http.MethodGet, Attend_table, query, nil)
	if err != nil {
		return nil, err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	attend := []*util.Attend{}
	err = json.NewDecoder(resp.Body).Decode(&attend)
	if err != nil {
		return nil, err
	}

	return attend, nil
}

func (q *repository) InsertAttend(user *util.User, meetingID int, role int) error {
	attend := &util.Attend{
		ID:        rand.Int(),
		UserID:    user.ID,
		MeetingID: meetingID,
		RoleID:    role,
		CreatedAt: time.Time{},
	}

	req, err := q.newRequest(http.MethodPost, Attend_table, "", attend)
	if err != nil {
		return err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil

}

func (q *repository) UpdateMeeting(query string, meeting *util.Meeting) error {
	req, err := q.newRequest(http.MethodPatch, Attend_table, query, meeting)
	if err != nil {
		return err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil

}

func (q *repository) DeleteMeetingAttend(query string, database string) error {
	req, err := q.newRequest(http.MethodDelete, database, query, nil)
	if err != nil {
		return err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil

}
