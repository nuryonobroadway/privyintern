package db

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/nuryonobroadway/privy-hardcode/util"
)

const (
	User_table    = "user"
	Meeting_table = "meeting"
	Session_table = "session"
	Attend_table  = "attend"
)

type (
	Repository interface {
		// user
		GetUser(string) ([]*util.User, error)
		GetUserByID(string) (*util.User, error)
		InsertUser(*util.User) error
		UpdatePassword(string, *util.User) error
		DeleteUser(string, int) error

		// session
		InsertSession(*util.User, string) error
		GetSessionByValue(string) ([]*util.Session, error)
		IsUserAlreadyLogin(string) (bool, error)
		UpdateSession(string, *util.Session) error
		DeleteSession(string) error

		// meeting
		InsertMeeting(string) (*util.Meeting, error)
		InsertAttend(*util.User, int, int) error
		GetMeetingByValue(string) ([]*util.Meeting, error)
		GetAttendByValue(string) ([]*util.Attend, error)
		UpdateMeeting(string, *util.Meeting) error
		DeleteMeetingAttend(string, string) error
	}

	repository struct {
		c      util.Config
		client *http.Client
	}
)

func NewInstance(config util.Config) Repository {
	return &repository{
		c:      config,
		client: &http.Client{},
	}
}

func (r repository) newRequest(mode, database, method string, body interface{}) (*http.Request, error) {
	var req *http.Request
	var err error

	switch mode {
	case http.MethodGet:
		// 'https://gezatuxvzeremzlmrfdk.supabase.co/rest/v1/user?select=*'
		url := r.c.ProjectURL + "/" + database + "?" + method
		req, err = http.NewRequest(mode, url, nil)
		if err != nil {
			return nil, err
		}

	case http.MethodPost:
		url := r.c.ProjectURL + "/" + database
		postBody, err := json.Marshal(body)
		if err != nil {
			return nil, err
		}

		responseBody := bytes.NewBuffer(postBody)
		req, err = http.NewRequest(mode, url, responseBody)
		if err != nil {
			return nil, err
		}

		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Prefer", "return=minimal")

	case http.MethodPatch:
		// 'https://gezatuxvzeremzlmrfdk.supabase.co/rest/v1/user?some_column=eq.someValue'

		url := r.c.ProjectURL + "/" + database + "?" + method
		postBody, err := json.Marshal(body)
		if err != nil {
			return nil, err
		}

		responseBody := bytes.NewBuffer(postBody)
		req, err = http.NewRequest(mode, url, responseBody)
		if err != nil {
			return nil, err
		}

		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Prefer", "return=minimal")

	case http.MethodDelete:
		// 'https://gezatuxvzeremzlmrfdk.supabase.co/rest/v1/user?some_column=eq.someValue'
		url := r.c.ProjectURL + "/" + database + "?" + method
		req, err = http.NewRequest(mode, url, nil)
		if err != nil {
			return nil, err
		}

	default:
		return nil, nil
	}

	req.Header.Add("apikey", r.c.ServiceRole)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %v", r.c.ServiceRole))

	return req, nil
}
