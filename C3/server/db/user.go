package db

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/nuryonobroadway/privy-hardcode/util"
)

func (q *repository) GetUser(query string) ([]*util.User, error) {
	req, err := q.newRequest(http.MethodGet, User_table, query, nil)
	if err != nil {
		return nil, err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	users := []*util.User{}
	err = json.NewDecoder(resp.Body).Decode(&users)
	if err != nil {
		return nil, err
	}

	return users, nil

}

func (q *repository) GetUserByID(query string) (*util.User, error) {
	user, err := q.GetUser(query)
	if err != nil {
		return nil, err
	}

	if len(user) == 0 {
		return nil, fmt.Errorf("user not found")
	}

	return user[0], nil
}

func (q *repository) InsertUser(user *util.User) error {
	availableUser, err := q.filterData("username", user.Username)
	if err != nil {
		return err
	}

	if len(availableUser) == 1 {
		return fmt.Errorf("username already available")
	}

	user.ID = rand.Int()
	user.CreatedAt = time.Now()
	user.UpdatedAt = time.Now()

	req, err := q.newRequest(http.MethodPost, User_table, "", user)
	if err != nil {
		return err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil

}

func (q *repository) UpdatePassword(query string, user *util.User) error {
	user.UpdatedAt = time.Now()
	req, err := q.newRequest(http.MethodPatch, User_table, query, user)
	if err != nil {
		return err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil

}

func (q *repository) DeleteUser(query string, id int) error {
	availableUser, err := q.filterData("id", id)
	if err != nil {
		return err
	}

	if availableUser == nil {
		return fmt.Errorf("user not found")
	}

	req, err := q.newRequest(http.MethodDelete, User_table, query, nil)
	if err != nil {
		return err
	}

	resp, err := q.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil

}

func (q *repository) filterData(target string, value any) ([]*util.User, error) {
	// curl 'https://gezatuxvzeremzlmrfdk.supabase.co/rest/v1/user?id=eq.1&select=*' \
	// -H "apikey: SUPABASE_KEY" \
	// -H "Authorization: Bearer SUPABASE_KEY" \
	// -H "Range: 0-9"

	filter := fmt.Sprintf("%v=eq.%v&select=*", target, value)
	user, err := q.GetUser(filter)
	if err != nil {
		return nil, err
	}

	return user, nil
}
